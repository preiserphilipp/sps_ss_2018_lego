package v4;

import java.util.ArrayList;
import java.util.List;

import lejos.hardware.Button;
import lejos.hardware.motor.Motor;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3IRSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.hardware.sensor.NXTColorSensor;
import lejos.robotics.RegulatedMotor;
import lejos.utility.Delay;
import v4.Drive;
import v4.Gripper;

public class Robot {
	
	// constants
	private final int DRIVING_SPEED = 350;
	private final float STABLE_HEIGHT_US = 0.05f;
	private final float STABLE_HEIGHT_IR = 0.00f;
	private final float NUMBER_OF_DETECTIONS = 2;
	
	// number of color detections
	public int yellowBlock = 0;
	public int greenBlock = 0;
	public int blueStable = 0;
	public int whiteStable = 0;
	
	// motors
	private RegulatedMotor grabMotor = Motor.A;
	public RegulatedMotor rightMotor = Motor.B;
	public RegulatedMotor leftMotor = Motor.C;
	
	// sensors
	public EV3UltrasonicSensor ev3USSensor = new EV3UltrasonicSensor(SensorPort.S1);
	private EV3ColorSensor ev3ColorSensor = new EV3ColorSensor(SensorPort.S3);
	public NXTColorSensor nxtColorSensor = new NXTColorSensor(SensorPort.S4);
	private EV3IRSensor irSensor = new EV3IRSensor(SensorPort.S2);
	
	// color values of objects (max. 3 cm apart)
	public enum BLOCK_COLOR_VALUES {
		// Winnie Pooh
		YELLOW_BLOCK (
				new float[] {0.03f, 0.45f}, // red -> min, max // new float[] {0.041f, 0.35f}
				new float[] {0.02f, 0.29f}, // green
				new float[] {0.0025f, 0.04f}), // blue // new float[] {0.0025f, 0.035f}
		// Esel
		GREEN_BLOCK (
				new float[] {0.001f, 0.045f}, // new float[] {0.004f, 0.04f}, 
				new float[] {0.02f, 0.15f},  //new float[] {0.01f, 0.15f},
				new float[] {0.001f, 0.03f}),
		BLUE_STABLE (
				new float[] {0.01f, 0.06f},
				new float[] {0.035f, 0.15f},
				new float[] {0.035f, 0.15f}),
		WHITE_STABLE (
				new float[] {0.15f, 0.4f}, // new float[] {0.02f, 0.15f},
				new float[] {0.15f, 0.4f}, // new float[] {0.02f, 0.15f},
				new float[] {0.07f, 0.2f}); // new float[] {0.01f, 0.12f});
		
		float red[];
		float green[];
		float blue[];
		
		private BLOCK_COLOR_VALUES(float red[], float green[], float blue[]) {
			this.red = red;
			this.green = green;
			this.blue = blue;
		}
		
		public float[][] getRGBBlockValues() {
			return new float[][] {red, green, blue};
		}
	}
	
	public enum AREA_COLOR_VALUES {
		// Esel
		WHITE_AREA (
				new float[] {0.38f, 0.7f},
				new float[] {0.35f, 0.7f},
				new float[] {0.3f, 0.6f},
				new float[] {0.08f, 0.5f}),
		// Winnie Pooh
		BLUE_AREA (
				new float[] {0.15f, 0.5f}, // 0.28f, 0.48f
				new float[] {0.25f, 0.55f}, // 0.12f, 0.45f
				new float[] {0.25f, 0.55f}, // 0.32f, 0.47f
				new float[] {0.05f, 0.5f}); // 0.05f, 0.45f
		
		float red[];
		float green[];
		float blue[];
		float unknown[];
		
		private AREA_COLOR_VALUES(float red[], float green[], float blue[], float unknow[]) {
			this.red = red;
			this.green = green;
			this.blue = blue;
			this.unknown = unknow;
		}
		
		public float[][] getRGBAreaValues() {
			return new float[][] {red, green, blue, unknown};
		}
	}
	
	public Robot() {
		ev3ColorSensor.setFloodlight(true);
		nxtColorSensor.setFloodlight(true);
		nxtColorSensor.setCurrentMode("ColorID");
		ev3ColorSensor.setCurrentMode("RGB");
	}
	
	//                                    TESTS
	// ----------------------------------------------------------------------------
	
	/**
	 * prints block color
	 */
	public void EV3BrickColorTest() {
		
		while(!Button.ENTER.isDown()) {
			
			float[] colorSample = getEV3ColorSample();
			
//			if (blockFound(colorSample, BLOCK_COLOR_VALUES.YELLOW_BLOCK)) { 
//				System.out.println("YELLOW");
//			} else if (blockFound(colorSample, BLOCK_COLOR_VALUES.GREEN_BLOCK)) { 
//				System.out.println("GREEN");
//			} else {
//				System.out.println("-");
//			}
			
			System.out.print("YELLOW: ");
			if (blockFound(colorSample, BLOCK_COLOR_VALUES.YELLOW_BLOCK)) {
				System.out.print("Y");
			} else {
				System.out.print("N");
			}
			
			System.out.print("\nGREEN: ");
			if (blockFound(colorSample, BLOCK_COLOR_VALUES.GREEN_BLOCK)) { 
				System.out.print("Y");
			} else {
				System.out.print("N");
			}
			
			System.out.print("\nBLUE: ");
			if (blockFound(colorSample, BLOCK_COLOR_VALUES.BLUE_STABLE)) {
				System.out.print("Y");
			} else {
				System.out.print("N");
			}
			
			System.out.print("\nWHITE: ");
			if (blockFound(colorSample, BLOCK_COLOR_VALUES.WHITE_STABLE)) {
				System.out.print("Y");
			} else {
				System.out.print("N");
			}
			
//			System.out.println("\n--------");
			System.out.println();
			
			double[] array = new double[] {-1.0, -1.0, -1.0, -1.0};
			int maxAt = 0;
			
			List<BLOCK_COLOR_VALUES> values = getMultipleDetections(colorSample);
			
			if (values.size() > 1) {
				for (BLOCK_COLOR_VALUES value : values) {
					
					double score = getScore(colorSample, value);
					
					if (value.toString().equals("YELLOW_BLOCK")) {
						System.out.println("Yellow score: " + score);
						array[0] = score;
					}
					if (value.toString().equals("GREEN_BLOCK")) {
						System.out.println("Green score: " + score);
						array[1] = score;
					}
					if (value.toString().equals("BLUE_STABLE")) {
						System.out.println("Blue score: " + score);
						array[2] = score;
					}
					if (value.toString().equals("WHITE_STABLE")) {
						System.out.println("White score: " + score);
						array[3] = score;
					}
				}

				for (int i = 0; i < array.length; i++) {
				    maxAt = array[i] > array[maxAt] ? i : maxAt;
				}
				System.out.println("guess: " + maxAt);
			}
			
			System.out.println("\n--------");
			
			Delay.msDelay(3000);
		}
	}
	
	/**
	 * prints area color
	 */
	public void NXTAreaColorTest() {
		
		while(true) {
			// break loop with ENTER
			if (Button.ENTER.isDown()) {
				System.out.println("Program was terminated by pressing ENTER.");
				grabAndWait(false);
				break;
			}
			
			if (Button.LEFT.isDown()) {
				grabAndWait(true);
			}
			if (Button.RIGHT.isDown()) {
				grabAndWait(false);
			}
			
			float[] colorSample = getNXTColorSample();
			
			if (grabMotor.getTachoCount() > 0) { // if gripper is in air
				if (areaFound(colorSample, AREA_COLOR_VALUES.BLUE_AREA)) { 
					System.out.println("BLUE");
				} else if (areaFound(colorSample, AREA_COLOR_VALUES.WHITE_AREA)) { 
					System.out.println("WHITE");
				} else {
					System.out.println("-");
				}
			}
			
		}
	}
	
	/**
	 * gripper moves up and down (for testing purposes)
	 */
	public void grabTest() {
		grabAndWait(true);
		Delay.msDelay(5000);
		grabAndWait(false);
	}
	
	// ----------------------------------------------------------------------------
	
	/**
	 * Loops until it finds a Brick
	 * @return true if the brick was yellow; false if not
	 */
	public boolean detectTargetBrick() {
		System.out.println("Put a Brick in front of EV3 Color Sensor!");
		while(true) {

			// break loop with ENTER
			if (Button.ENTER.isDown()) {
				System.out.println("Program was terminated by pressing ENTER.");
				return false;
			}

			
			float[] colorSample = getEV3ColorSample();
			
			if (blockFound(colorSample, BLOCK_COLOR_VALUES.YELLOW_BLOCK)) {
				yellowBlock++;
			} else {
				yellowBlock = 0;
			}
			if (blockFound(colorSample, BLOCK_COLOR_VALUES.GREEN_BLOCK)) {
				greenBlock++;
			} else {
				greenBlock = 0;
			}
			
			int numberOfDetections = 50;
			
//			System.out.println(yellowBlock);
			
			if (blockFound(colorSample, BLOCK_COLOR_VALUES.YELLOW_BLOCK) && yellowBlock > numberOfDetections) { 
				System.out.println("YELLOW");
				System.out.println(colorSample[0]);
				System.out.println(colorSample[1]);
				System.out.println(colorSample[2]);
				resetNumberOfDetections();
				return true;
			} 
			else if (blockFound(colorSample, BLOCK_COLOR_VALUES.GREEN_BLOCK) && greenBlock > numberOfDetections) { 
				System.out.println("GREEN");
				System.out.println(colorSample[0]);
				System.out.println(colorSample[1]);
				System.out.println(colorSample[2]);
				resetNumberOfDetections();
				return false;
			}
			
			Delay.msDelay(20);
		}
	}

	/**
	 * Robot drives around randomly. Checks regularly if the robot is
	 * about to drive of the edge. If so, the robot drives backwards and changes
	 * its angle randomly. 
	 * If a block is detected the gripper grabs it. The robot then searches for an 
	 * area with a certain color to place the block in it.
	 */
	public void driveRandom() {
		nxtColorSensor.setCurrentMode("ColorID");
		float[] nxtColorSensorSample = new float[nxtColorSensor.sampleSize()];
		nxtColorSensor.fetchSample(nxtColorSensorSample, 0);
		
		ev3ColorSensor.setCurrentMode("RGB");
		float[] ev3ColorSensorSample = new float[ev3ColorSensor.sampleSize()];
		ev3ColorSensor.fetchSample(ev3ColorSensorSample, 0);
		
		ev3ColorSensor.setFloodlight(true);
		
		boolean yellowBlockSelected = false;
		boolean blockFound = false;
		
		setDrivingSpeed(DRIVING_SPEED);
		
		yellowBlockSelected = detectTargetBrick();
		
		int countdown = 5;
		while (countdown != 0) {
			System.out.println(countdown);
			countdown--;
			Delay.msDelay(1000);
		}
//		Delay.msDelay(5000);
		
		while(true) {
			
			// breaks loop with ENTER
			if (Button.ENTER.isDown()) {
				grabAndWait(false); // move gripper to start position
				System.out.println("Program was terminated by pressing ENTER.");
				break;
			}
			
			// drive thread
			Thread drive = new Thread(getDrive()); // robot drives around randomly (and detects ledge)
			drive.start();
			
			if(!blockFound) {

				ev3ColorSensorSample = getEV3ColorSample(); // EV3 color sensor (food)
				
//				if (blockFound(ev3ColorSensorSample, BLOCK_COLOR_VALUES.YELLOW_BLOCK)) {
//					yellowBlock++;
//				} else if (blockFound(ev3ColorSensorSample, BLOCK_COLOR_VALUES.GREEN_BLOCK)) {
//					greenBlock++;
//				} else if (blockFound(ev3ColorSensorSample, BLOCK_COLOR_VALUES.BLUE_STABLE)) {
//					blueStable++;
//				} else if (blockFound(ev3ColorSensorSample, BLOCK_COLOR_VALUES.WHITE_STABLE)) {
//					whiteStable++;
//				} else {
//					yellowBlock = 0;
//					greenBlock = 0;
//					blueStable = 0;
//					whiteStable = 0;
//				}
				
				
				if (blockFound(ev3ColorSensorSample, BLOCK_COLOR_VALUES.YELLOW_BLOCK)) {
					yellowBlock++;
				} else {
					yellowBlock = 0;
				}
				if (blockFound(ev3ColorSensorSample, BLOCK_COLOR_VALUES.GREEN_BLOCK)) {
					greenBlock++;
				} else {
					greenBlock = 0;
				}
				if (blockFound(ev3ColorSensorSample, BLOCK_COLOR_VALUES.BLUE_STABLE)) {
					blueStable++;
				} else {
					blueStable = 0;
				}
				if (blockFound(ev3ColorSensorSample, BLOCK_COLOR_VALUES.WHITE_STABLE)) {
					whiteStable++;
				} else {
					whiteStable = 0;
				}
				
				double[] array = new double[] {-1.0, -1.0, -1.0, -1.0};
				int maxAt = -1;
				
				List<BLOCK_COLOR_VALUES> values = getMultipleDetections(ev3ColorSensorSample);
				
				if (values.size() > 1) {
					for (BLOCK_COLOR_VALUES value : values) {
						
						double score = getScore(ev3ColorSensorSample, value);
						
						if (value.toString().equals("YELLOW_BLOCK")) {
//							System.out.println("Yellow score: " + score);
							array[0] = score;
						}
						if (value.toString().equals("GREEN_BLOCK")) {
//							System.out.println("Green score: " + score);
							array[1] = score;
						}
						if (value.toString().equals("BLUE_STABLE")) {
//							System.out.println("Blue score: " + score);
							array[2] = score;
						}
						if (value.toString().equals("WHITE_STABLE")) {
//							System.out.println("White score: " + score);
							array[3] = score;
						}
					}
					
					maxAt = 0;
					for (int i = 0; i < array.length; i++) {
					    maxAt = array[i] > array[maxAt] ? i : maxAt;
					}
//					System.out.println("guess: " + maxAt);
				}
				
				// if correct block (= food) was found
				if ((yellowBlockSelected && yellowBlock >= NUMBER_OF_DETECTIONS /*&& (maxAt == 0 || maxAt == -1)*/ && (array[1] == -1.0))
						|| (!yellowBlockSelected && greenBlock >= 1 && (maxAt == 1 || maxAt == -1))) {
					System.out.println("Correct Block Found!");
					resetNumberOfDetections();
					grabAndWait(true);
					drive.interrupt();
					blockFound = true;
				}else {
					//if correct block was not found, but there was still a brick/stable found, avoid it
					if ((yellowBlock >= NUMBER_OF_DETECTIONS /*&& (maxAt == 0 || maxAt == -1)*/)
						|| (greenBlock >= NUMBER_OF_DETECTIONS && (maxAt == 1 || maxAt == -1))
						|| (blueStable >= NUMBER_OF_DETECTIONS /*&& (maxAt == 2 || maxAt == -1)*/)
						|| (whiteStable >= NUMBER_OF_DETECTIONS /*&& (maxAt == 3 || maxAt == -1)*/)) {
						System.out.println("Incorrect Block Found!");
						resetNumberOfDetections();
//						System.out.println(ev3ColorSensorSample[0]);
//						System.out.println(ev3ColorSensorSample[1]);
//						System.out.println(ev3ColorSensorSample[2]);
						driveBackwardsAndTurn(1000);
					}
				}
				
			} else {
				//For testing purposes: While Gripper is up, pressing Right will drop the Brick
				if (Button.RIGHT.isDown()) {
					grabAndWait(false);
					System.out.println("Dropping Brick");
					blockFound = false;
				}
				//Actual way to drop the Brick: check if the correct stable color was found
				if(feedingPlaceFound(yellowBlockSelected)) {
					grabAndWait(false);
					driveBackwardsAndTurn(3000);
					blockFound = false;
				}
			}
			
			try {
				drive.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * not fully implemented yet
	 * @param yellowBlockSelected Parameter that specifies which stable-color should be detected. 
	 * @return true if stable was found
	 */
	public boolean feedingPlaceFound(boolean yellowBlockSelected) {
		if (stableFound()) { // stable found with us or ir sensor
			float[] nxtColorSensorSample = new float[nxtColorSensor.sampleSize()];
			nxtColorSensor.fetchSample(nxtColorSensorSample, 0);
			if ((!yellowBlockSelected && Float.compare(nxtColorSensorSample[0], 6.0f) == 0  || 
					(yellowBlockSelected && Float.compare(nxtColorSensorSample[0], 2.0f) == 0))) {
				return true;
			} else if ((yellowBlockSelected && Float.compare(nxtColorSensorSample[0], 6.0f) == 0  || 
					(!yellowBlockSelected && Float.compare(nxtColorSensorSample[0], 2.0f) == 0))){
				driveBackwardsAndTurn(3000);
			}
		}
		return false;
	}
	
	public List<BLOCK_COLOR_VALUES> getMultipleDetections(float[] colorSample) {
		
		List<BLOCK_COLOR_VALUES> detections = new ArrayList<BLOCK_COLOR_VALUES>();
		
		if (blockFound(colorSample, BLOCK_COLOR_VALUES.YELLOW_BLOCK)) {
			detections.add(BLOCK_COLOR_VALUES.YELLOW_BLOCK);
		}
		if (blockFound(colorSample, BLOCK_COLOR_VALUES.GREEN_BLOCK)) { 
			detections.add(BLOCK_COLOR_VALUES.GREEN_BLOCK);
		}
		if (blockFound(colorSample, BLOCK_COLOR_VALUES.BLUE_STABLE)) {
			detections.add(BLOCK_COLOR_VALUES.BLUE_STABLE);
		}
		if (blockFound(colorSample, BLOCK_COLOR_VALUES.WHITE_STABLE)) {
			detections.add(BLOCK_COLOR_VALUES.WHITE_STABLE);
		}
		
		return detections;
	}
	
	public double getScore(float[] colorSample, BLOCK_COLOR_VALUES values) {
		double val1 = 0;
		double val2 = 0;
		double val3 = 0;
		
		double score = 0;
		
		for (int i=0; i<values.getRGBBlockValues().length; i++) {
			val1 = values.getRGBBlockValues()[i][1] - values.getRGBBlockValues()[i][0];
			val2 = values.getRGBBlockValues()[i][1] - colorSample[i];
			val3 = colorSample[i] - values.getRGBBlockValues()[i][0];
			
			if (val1 >= (val2 + val3)) {
				score += (val1 - (val2 + val3));
			} else {
				score += ((val2 + val3) - val1);
			}
			
		}
		
		return score;
//		return score/3;
	}
	
	/**
	 * Activates gripper and sets speed of robot to a low level.
	 * @param closeGripper Open or close gripper.
	 */
	public void grabAndWait(boolean closeGripper) {
		// slows robot down while it grabs a block
		setDrivingSpeed(10);
		
		Thread grab = new Thread(new Gripper(grabMotor, closeGripper));
		grab.start();
		
		// drive backwards while gripper opens
		if (!closeGripper) {
			Delay.msDelay(2500); // wait while gripper puts brick on stable
			setDrivingSpeed(100);
			leftMotor.backward();
			rightMotor.backward();
		}
		
		try {
			grab.join();
			Delay.msDelay(2000); // drive away backwards from stable
			// set to normal speed
			setDrivingSpeed(DRIVING_SPEED);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Sets driving speed of robot.
	 * @param speed Speed in which the robot should move
	 */
	public void setDrivingSpeed(int speed) {
		leftMotor.setSpeed(speed);
		rightMotor.setSpeed(speed);
	}
	
	/**
	 * 
	 * @return IR sensor value
	 */
	public float[] getIRSensorSample() {
		float[] irSample = new float[irSensor.sampleSize()];
		irSensor.fetchSample(irSample, 0);
		return irSample;
	}
	
	/**
	 * 
	 * @return US sensor value
	 */
	public float[] getUSSensorSample() {
		float[] usSample = new float[ev3USSensor.sampleSize()];
		ev3USSensor.fetchSample(usSample, 0);
		return usSample;
	}
	
	/**
	 * 
	 * @return RGB value sample of EV3 color sensor
	 */
	public float[] getEV3ColorSample() {
		ev3ColorSensor.setCurrentMode("RGB");
		float[] ev3ColorSensorSample = new float[ev3ColorSensor.sampleSize()];
		ev3ColorSensor.fetchSample(ev3ColorSensorSample, 0);
		return ev3ColorSensorSample;
	}
	
	/**
	 * 
	 * @return RGB value sample of NXT color sensor
	 */
	public float[] getNXTColorSample() {
		nxtColorSensor.setCurrentMode("RGB");
		float[] nxtColorSensorSample = new float[nxtColorSensor.sampleSize()];
		nxtColorSensor.fetchSample(nxtColorSensorSample, 0);
		return nxtColorSensorSample;
	}
	
	/**
	 * 
	 * @param colorSample sample of the EV3 color sensor in RGB mode
	 * @param colorThreshold RGB color range of specified LEGO block
	 * @return true if color sample values of EV3 color sensor are within the range of the specified block
	 */
	public boolean blockFound(float[] colorSample, BLOCK_COLOR_VALUES colorThreshold) {
		for (int i=0; i<colorThreshold.getRGBBlockValues().length; i++) {
			if (colorSample[i] <= colorThreshold.getRGBBlockValues()[i][0]
					|| colorSample[i] >= colorThreshold.getRGBBlockValues()[i][1]) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 
	 * @param colorSample sample of the NXT color sensor in RGB mode
	 * @param colorThreshold RGB color range of specified area
	 * @return true if color sample values of NXT color sensor are within the range of the specified area
	 */
	public boolean areaFound(float[] colorSample, AREA_COLOR_VALUES color_THRESHOLD) {
		for (int i=0; i<color_THRESHOLD.getRGBAreaValues().length; i++) {
			if (colorSample[i] <= color_THRESHOLD.getRGBAreaValues()[i][0]
					|| colorSample[i] >= color_THRESHOLD.getRGBAreaValues()[i][1]) {
				return false;
			}
		}
		return true;
	}
	
	public boolean stableFound() {
		float usSample = getUSSensorSample()[0];
		if (getIRSensorSample()[0] <= STABLE_HEIGHT_IR || usSample <= STABLE_HEIGHT_US || Float.toString(usSample).equals("Infinity")) {
			System.out.println("STABLE FOUND");
			return true;
		}
		return false;
	}
	
	/**
	 * Robot drives backwards and turns in random direction
	 * */
	public void driveBackwardsAndTurn(int rotationTime) {
		leftMotor.backward();
		rightMotor.backward();
		Delay.msDelay(500);
		leftMotor.forward();
		rightMotor.backward();
		Delay.msDelay(rotationTime);
	}
	
	public Drive getDrive() {
		return new Drive(rightMotor, leftMotor, irSensor, ev3USSensor);
	}
	
	public void resetNumberOfDetections() {
		yellowBlock = 0;
		greenBlock = 0;
		blueStable = 0;
		whiteStable = 0;
	}
	
	/**
	 * stops and close all motors
	 */
	public void stopMotors() {
//		setDrivingSpeed(10);
		leftMotor.stop();
		rightMotor.stop();
		rightMotor.close();
		leftMotor.close();
	}

	/**
	 * closes all sensors
	 */
	public void closeSensors() {
		ev3USSensor.close();
		ev3ColorSensor.close();
		nxtColorSensor.close();
		irSensor.close();
	}
}