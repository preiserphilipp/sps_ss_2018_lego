package v4;

public class Main {

	public static void main(String[] args) {
		Robot robot = new Robot();
		try {
//			robot.grabTest();
			robot.driveRandom();
//			robot.EV3BrickColorTest();
		} 
		finally {
			robot.stopMotors();
			robot.closeSensors();
		}
	}
}
