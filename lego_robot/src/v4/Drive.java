package v4;

import java.util.concurrent.ThreadLocalRandom;

import lejos.hardware.sensor.EV3IRSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.RegulatedMotor;
import lejos.utility.Delay;

public class Drive implements Runnable {
	
	private final float LEDGE_HEIGHT_US = 0.3f;
	private final float LEDGE_HEIGHT_IR = 30.0f;
	private final float STABLE_HEIGHT = 0.03f;
	
	private EV3IRSensor irSensor;
	private EV3UltrasonicSensor ev3USSensor;

	private RegulatedMotor rightMotor;
	private RegulatedMotor leftMotor;
	
	public Drive(RegulatedMotor rightMotor, RegulatedMotor leftMotor, EV3IRSensor irSensor, EV3UltrasonicSensor ev3USSensor) {
		this.rightMotor = rightMotor;
		this.leftMotor = leftMotor;
		
		this.irSensor = irSensor;
		this.ev3USSensor = ev3USSensor;
	}
	
	@Override
	public void run() {
		if(hasUSSensorDetectedLedge()) {
			driveBackwardsAndTurnUS();
		} else if (hasIRSensorDetectedLedge()) {
			driveBackwardsAndTurnIR();
		} else {
			drive();
		}
	}
	
	/**
	 * 
	 * @return true if the Ultrasonic sensor detects a ledge or a stable. Only detects a 
	 */
	public boolean hasUSSensorDetectedLedge() {
		float[] usSample = new float[ev3USSensor.sampleSize()];
		ev3USSensor.fetchSample(usSample, 0);
		return usSample[0] > LEDGE_HEIGHT_US || usSample[0] < STABLE_HEIGHT;
	}
	
	/**
	 * 
	 * @return true if the IR sensor detects a ledge 
	 */
	public boolean hasIRSensorDetectedLedge() {
		float[] irSample = new float[irSensor.sampleSize()];
		irSensor.fetchSample(irSample, 0);
		return irSample[0] > LEDGE_HEIGHT_IR;
	}
	
	public void drive() {
		leftMotor.forward();
		rightMotor.forward();
		Delay.msDelay(500);
	}
	
	public void driveBackwardsAndTurnUS() {
		leftMotor.backward();
		rightMotor.backward();
		Delay.msDelay(500);
		leftMotor.forward();
		rightMotor.backward();
		Delay.msDelay(ThreadLocalRandom.current().nextInt(300, 1000));
	}
	
	public void driveBackwardsAndTurnIR() {
		leftMotor.backward();
		rightMotor.backward();
		Delay.msDelay(500);
		leftMotor.backward();
		rightMotor.forward();
		Delay.msDelay(ThreadLocalRandom.current().nextInt(300, 1000));
	}

}
