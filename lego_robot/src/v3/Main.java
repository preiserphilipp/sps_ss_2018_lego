package v3;

public class Main {

	public static void main(String[] args) {
		Robot robot = new Robot();
		try {
			robot.driveRandom();
		} 
		finally {
			robot.closeSensors();
		}
	}
}
