package v2;

import lejos.utility.Delay;

public class Alpha_Test {
	
	Robot robot = new Robot();
	
	public void testDriveForward(){
		robot.leftMotor.forward();
		robot.rightMotor.forward();
		Delay.msDelay(2000);
		closeSensors();
	}
	
	/**/
	public void testFindEdgeNXTColor() {//Use Color Values from the Test Arena
		boolean edge = true;
		while (edge) {
			float[] colorSample = robot.getNXTColorSample();
			for(float sample : colorSample) {
				if(!(sample >= 0.35f && sample < 0.55f)) {
					System.out.println(sample);
					edge = false;
				}
			}
			if(edge) {
				System.out.println("Edge Found"); 
			}
			else {
				System.out.println("Edge Not Found"); 
			}
		}
		closeSensors();
	}
	
	/**/
	public void testFindEdgeUltrasonic() {

		Drive d = robot.getDrive();
		if(d.hasUSSensorDetectedLedge()) {
			System.out.println("Edge Found");
		}
		else {
			System.out.println("Edge not found");
		}
		closeSensors();
	}
	
	/**/
	public void testOutputColorBrick(){
		robot.EV3BrickColorTest();
		closeSensors();
	}
	
	public void testOutputColorGroundWhileUp() {
		robot.grabAndWait(true);
		robot.NXTAreaColorTest();
		robot.grabAndWait(false);;
		closeSensors();
	}
	
	/**/
	public void testLiftBrick() {
		robot.grabTest();
		closeSensors();
	}
	
	/**/
	public void testDetectEnemyGripperDown() {
		robot.EV3BrickColorTest();
		closeSensors();
	}
	
	/**/
	public void testDetectStablesWithUltraSonic() {
		robot.grabAndWait(true);
		if(robot.getUSSensorSample()[0] <= 0.05) {
			System.out.println("Stable detected");
		}
		robot.grabAndWait(false);
		closeSensors();
	}
	
	public void testDropBrickIfGroundBlue() {
		robot.grabAndWait(true);
		while(true) {
			if(robot.feedingPlaceFound(false)) {
				robot.grabAndWait(false);
				break;
			}
		}
		closeSensors();
	}
	
	public void testDropBrickIfGroundYellow() {
		robot.grabAndWait(true);
		while(true) {
			if(robot.feedingPlaceFound(true)) {
				robot.grabAndWait(false);
				break;
			}
		}
		closeSensors();
	}
	
	/**
	 */
	public void testFindObstacleInfrared() {
		robot.grabAndWait(true);
		if(robot.getIRSensorSample()[0] <= 0.0) {
			System.out.println("Obstacle detected");
		}
		robot.grabAndWait(false);
		closeSensors();
	}

	public void testFindObstacleWhileDown() {
		robot.NXTAreaColorTest();
		closeSensors();
		
	}
	
	private void closeSensors() {
		robot.closeSensors();
	}
}
