package v2;

import java.util.concurrent.ThreadLocalRandom;

import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.hardware.sensor.NXTColorSensor;
import lejos.robotics.RegulatedMotor;
import lejos.utility.Delay;

public class Drive implements Runnable {
	
	private final double LEDGE_HEIGHT = 0.30;
	
	private NXTColorSensor nxtColorSensor;
	private EV3UltrasonicSensor ev3USSensor;

	private RegulatedMotor rightMotor;
	private RegulatedMotor leftMotor;
	
	public Drive(RegulatedMotor rightMotor, RegulatedMotor leftMotor, NXTColorSensor nxtColorSensor, EV3UltrasonicSensor ev3USSensor) {
		this.rightMotor = rightMotor;
		this.leftMotor = leftMotor;
		
		this.nxtColorSensor = nxtColorSensor;
		this.ev3USSensor = ev3USSensor;
	}
	
	@Override
	public void run() {
		if(!hasUSSensorDetectedLedge() && !hasNXTColorSensorDetectedLedge()) { // no ledge
			drive();
		} else { // ledge detected
			driveBackwardsAndTurn();
		}
	}
	
	/**
	 * 
	 * @return true if the IR Sensor measures a distance > than the Ledge Height
	 */
	public boolean hasUSSensorDetectedLedge() {
		float[] usSample = new float[ev3USSensor.sampleSize()];
		ev3USSensor.fetchSample(usSample, 0);
		return usSample[0] > LEDGE_HEIGHT;
	}
	
	/**
	 * 
	 * @return true if the NXT color sensor values are within a certain range
	 */
	public boolean hasNXTColorSensorDetectedLedge() {
		nxtColorSensor.setCurrentMode("RGB");
		float[] colorSample = new float[nxtColorSensor.sampleSize()];
		nxtColorSensor.fetchSample(colorSample, 0);
		for(float sample : colorSample) {
//			System.out.println("color: " + sample); // for debugging purposes
			if(!(sample >= 0.1f && sample < 0.3f)) {
				return false; 
			}
		}
//		System.out.println("_______"); // for debugging purposes
		return true;
	}
	
	public void drive() {
		leftMotor.forward();
		rightMotor.forward();
		Delay.msDelay(500);
	}
	
	public void driveBackwardsAndTurn() {
		leftMotor.backward();
		rightMotor.backward();
		Delay.msDelay(500);
		leftMotor.forward();
		rightMotor.backward();
		Delay.msDelay(ThreadLocalRandom.current().nextInt(500, 1500));
	}

}
