package v2;

import java.util.concurrent.ThreadLocalRandom;

import lejos.hardware.Button;
import lejos.hardware.motor.Motor;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3IRSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.hardware.sensor.NXTColorSensor;
import lejos.robotics.RegulatedMotor;
import lejos.utility.Delay;
import v2.Gripper;

public class Robot {
	
	// constants
	private final int DRIVING_SPEED = 400;
	private final double MAX_DISTANCE_OBSTACLE = 0.0;
	private final double MAX_DISTANCE_FEEDING_PLACE = 0.04;
	
	// motors
	private RegulatedMotor grabMotor = Motor.A;
	public RegulatedMotor rightMotor = Motor.B;
	public RegulatedMotor leftMotor = Motor.C;
	
	// sensors
	public EV3UltrasonicSensor ev3USSensor = new EV3UltrasonicSensor(SensorPort.S1);
	private EV3ColorSensor ev3ColorSensor = new EV3ColorSensor(SensorPort.S2);
	public NXTColorSensor nxtColorSensor = new NXTColorSensor(SensorPort.S3);
	private EV3IRSensor irSensor = new EV3IRSensor(SensorPort.S4);
	
	// color values of objects (max. 3 cm apart)
	public enum BLOCK_COLOR_VALUES {
		YELLOW_BLOCK (
				new float[] {0.06f, 0.35f}, // red -> min, max
				new float[] {0.02f, 0.2f}, // green
				new float[] {0.0025f, 0.035f}), // blue
		GREEN_BLOCK (
				new float[] {0.004f, 0.04f}, 
				new float[] {0.01f, 0.15f}, 
				new float[] {0.001f, 0.03f}),
		BLUE_BLOCK (
				new float[] {0.005f, 0.055f},
				new float[] {0.01f, 0.3f},
				new float[] {0.01f, 0.12f});
		
		float red[];
		float green[];
		float blue[];
		
		private BLOCK_COLOR_VALUES(float red[], float green[], float blue[]) {
			this.red = red;
			this.green = green;
			this.blue = blue;
		}
		
		public float[][] getRGBBlockValues() {
			return new float[][] {red, green, blue};
		}
	}
	
	public enum AREA_COLOR_VALUES {
		GREEN_AREA (
				new float[] {0.2f, 0.45f},
				new float[] {0.13f, 0.55f},
				new float[] {0.25f, 0.44f},
				new float[] {0.07f, 0.55f}),
		YELLOW_AREA (
				new float[] {0.35f, 0.67f}, // 0.5f, 0.67f
				new float[] {0.25f, 0.55f}, // 0.4f, 0.55f
				new float[] {0.17f, 0.47f}, // 0.36f, 0.47f
				new float[] {0.05f, 0.45f}), // 0.05f, 0.45f
		BLUE_AREA (
				new float[] {0.12f, 0.48f}, // 0.28f, 0.48f
				new float[] {0.12f, 0.45f}, // 0.12f, 0.45f
				new float[] {0.14f, 0.47f}, // 0.32f, 0.47f
				new float[] {0.05f, 0.45f}); // 0.05f, 0.45f
		
		float red[];
		float green[];
		float blue[];
		float unknown[];
		
		private AREA_COLOR_VALUES(float red[], float green[], float blue[], float unknow[]) {
			this.red = red;
			this.green = green;
			this.blue = blue;
			this.unknown = unknow;
		}
		
		public float[][] getRGBAreaValues() {
			return new float[][] {red, green, blue, unknown};
		}
	}
	
	public Robot() {
		ev3ColorSensor.setFloodlight(true);
		nxtColorSensor.setFloodlight(true);
	}
	
	//                                    TESTS
	// ----------------------------------------------------------------------------
	
	/**
	 * prints block color
	 */
	public void EV3BrickColorTest() {
		
		while(true) {
			// break loop with ENTER
			if (Button.ENTER.isDown()) {
				System.out.println("Program was terminated by pressing ENTER.");
				break;
			}
			
			float[] colorSample = getEV3ColorSample();
			
			if (blockFound(colorSample, BLOCK_COLOR_VALUES.YELLOW_BLOCK)) { 
				System.out.println("YELLOW");
			} else if (blockFound(colorSample, BLOCK_COLOR_VALUES.GREEN_BLOCK)) { 
				System.out.println("GREEN");
			} else if(blockFound(colorSample, BLOCK_COLOR_VALUES.BLUE_BLOCK)) {
				System.out.println("ENEMY");
			}else {
				System.out.println("-");
			}
		}
	}
	
	/**
	 * prints area color
	 */
	public void NXTAreaColorTest() {
		
		while(true) {
			// break loop with ENTER
			if (Button.ENTER.isDown()) {
				System.out.println("Program was terminated by pressing ENTER.");
				grabAndWait(false);
				break;
			}
			
			if (Button.LEFT.isDown()) {
				grabAndWait(true);
			}
			if (Button.RIGHT.isDown()) {
				grabAndWait(false);
			}
			
			float[] colorSample = getNXTColorSample();
			
			if (grabMotor.getTachoCount() > 0) { // if gripper is in air
				if (areaFound(colorSample, AREA_COLOR_VALUES.BLUE_AREA)) { 
					System.out.println("BLUE");
				} else if (areaFound(colorSample, AREA_COLOR_VALUES.YELLOW_AREA)) { 
					System.out.println("YELLOW");
				} else {
					System.out.println("-");
				}
			} else { // if gripper is on ground
				if (areaFound(colorSample, AREA_COLOR_VALUES.GREEN_AREA)) { 
					System.out.println("GREEN");
				} else {
					System.out.println("-");
				}
			}
			
		}
	}
	
	/**
	 * gripper moves up and down (for testing purposes)
	 */
	public void grabTest() {
		grabAndWait(true);
		Delay.msDelay(5000);
		grabAndWait(false);
	}
	
	// ----------------------------------------------------------------------------

	/**
	 * Robot drives around randomly. Checks regularly if the robot is
	 * about to drive of the edge. If so, the robot drives backwards and changes
	 * its angle randomly. 
	 * If a block is detected the gripper grabs it. The robot then searches for an 
	 * area with a certain color to place the block in it.
	 * 
	 * @param speed
	 *            Speed in which the robot should move
	 */
	public void driveRandomAndDoStuff() {
		boolean yellowBlockSelected = false;
		
		boolean yellowBlockFound = false;
		boolean greenBlockFound = false;
		
//		boolean isSuppostToBeInGreenArea = false;
//		boolean avoidGreenArea = false;
		
		setDrivingSpeed(DRIVING_SPEED);
		
		// food selection
		System.out.println("Press LEFT key for yellow block and RIGHT key for green block.");
		Button.waitForAnyPress();
		if (Button.LEFT.isDown()) {
			yellowBlockSelected = true;
			System.out.println("YELLOW selected");
		}
		if (Button.RIGHT.isDown()) {
			yellowBlockSelected = false;
			System.out.println("GREEN selected");
		}
		Delay.msDelay(3000);
		
		
		while(true) {
			
			// breaks loop with ENTER
			if (Button.ENTER.isDown()) {
				grabAndWait(false);// move gripper to start position
				System.out.println("Program was terminated by pressing ENTER.");
				break;
			}
			
			// drive thread
			Thread drive = new Thread(getDrive()); // robot drives around randomly (and detects ledge)
			drive.start();
			
//			avoidObstacles();
			
			if (!yellowBlockFound && !greenBlockFound) { // if no brick is in gripper (= no food found)
				
				// if robot has green selected and found green area and is no longer in green area -> turn and continue searching for green food
				/* green block can be anywhere on the field (but it is more likely that it is in the green area) */
				// experimental
//				if (!yellowBlockSelected && areaFound(getNXTColorSample(), AREA_COLOR_VALUES.GREEN_AREA) && !greenBlockFound && !isSuppostToBeInGreenArea) {
//					isSuppostToBeInGreenArea = true;
//				}
//				if (isSuppostToBeInGreenArea && !areaFound(getNXTColorSample(), AREA_COLOR_VALUES.GREEN_AREA)) {
//					turnAndDriveBackwards();
//				}
				
				float [] ev3ColorSensorSample = getEV3ColorSample(); // EV3 color sensor (food)
				// if block (= food) was found
				if ((yellowBlockSelected && blockFound(ev3ColorSensorSample, BLOCK_COLOR_VALUES.YELLOW_BLOCK))
						|| (!yellowBlockSelected && blockFound(ev3ColorSensorSample, BLOCK_COLOR_VALUES.GREEN_BLOCK))) { 
					
//					isSuppostToBeInGreenArea = false;
//					avoidGreenArea = true;
					
					grabAndWait(true);
					
					if (yellowBlockSelected) {
						yellowBlockFound = true;
					} else {
						greenBlockFound = true;
					}
					
//					try {
						// check if gripper has taken hold of block (in air), if not - open gripper
						// experimental 
//						if ((yellowBlockFound && !blockFound(getEV3ColorSample(), BLOCK_COLOR_VALUES.YELLOW_BLOCK)) ||
//								(greenBlockFound && !blockFound(getEV3ColorSample(), BLOCK_COLOR_VALUES.GREEN_BLOCK))) {
//							grab = new Thread(new Gripper(grabMotor, false));
//							grab.start();
//							grab.join();
//							
//							yellowBlockFound = false;
//							greenBlockFound = false;
//						}		
						
						drive.interrupt();
//						drive.join();
//					} catch (InterruptedException e) {
//						e.printStackTrace();
//					}
				} else { // if no block found
					ev3ColorSensorSample = getEV3ColorSample();
					// if desired brick was not found, avoid every brick
					if (blockFound(ev3ColorSensorSample, BLOCK_COLOR_VALUES.YELLOW_BLOCK) 
						|| blockFound(ev3ColorSensorSample, BLOCK_COLOR_VALUES.GREEN_BLOCK)) {
						driveBackwardsAndTurn();
					}
				}
			}
			
			// if food is in gripper, search for feeding place
			if (yellowBlockFound || greenBlockFound) {
				if (feedingPlaceFound(yellowBlockFound)) {
					grabAndWait(false);
					break;
				}
			}
		}
	}
	
	/**
	 * not fully implemented yet
	 * @param yellowBlockFound Parameter that specifies which stable-color should be detected. 
	 * @return true if stable was found
	 */
	public boolean feedingPlaceFound(boolean yellowBlockFound) {
		/* TODO: find feeding place
		 * IR sensor (to detect elevation)
		 * UR sensor (to detect different distance to the ground) 
		 * NXT color sensor (for color of feeding place)
		 */
		
		float[] colorSample = getNXTColorSample();
		
//		if (getUSSensorSample()[0] <= MAX_DISTANCE_FEEDING_PLACE) { // if US sensor is over feeding place TODO: not tested
			// if selected color of feeding place was found
			if ((!yellowBlockFound && areaFound(colorSample, AREA_COLOR_VALUES.BLUE_AREA)) || 
					(yellowBlockFound && areaFound(colorSample, AREA_COLOR_VALUES.YELLOW_AREA))) {
				return true;
			}
//		}
		return false;
	}
	
	/**
	 * Avoids obstacles (animals) via the EV3 color sensor if gripper is open or via the IR sensor if the gripper is closed
	 * not fully implemented yet
	 */
	public void avoidObstacles() {
		if (grabMotor.getTachoCount() <= 0) { // gripper is open
			// TODO: avoid animals via EV3 color sensor (not: trees -> honey)
		} else { // gripper is closed (avoid every obstacle)
			if (getIRSensorSample()[0] <= MAX_DISTANCE_OBSTACLE) { //avoid obstacles with IR sensor
				driveBackwardsAndTurn(); //TODO: fence and feeding place?
			}
		}
	}
	
	/**
	 * Activates gripper and sets speed of robot to a low level.
	 * @param closeGripper Open or close gripper.
	 */
	public void grabAndWait(boolean closeGripper) {
		// slows robot down while it grabs a block
		setDrivingSpeed(10);
		
		Thread grab = new Thread(new Gripper(grabMotor, closeGripper));
		grab.start();
		
		// drive backwards while gripper opens
		if (!closeGripper) {
			setDrivingSpeed(100);
			leftMotor.backward();
			rightMotor.backward();
		}
		
		try {
			grab.join();
			
			// set to normal speed
			setDrivingSpeed(DRIVING_SPEED);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Sets driving speed of robot.
	 * @param speed Speed in which the robot should move
	 */
	public void setDrivingSpeed(int speed) {
		leftMotor.setSpeed(speed);
		rightMotor.setSpeed(speed);
	}
	
	/**
	 * 
	 * @return IR sensor value
	 */
	public float[] getIRSensorSample() {
		float[] irSample = new float[irSensor.sampleSize()];
		irSensor.fetchSample(irSample, 0);
		return irSample;
	}
	
	/**
	 * 
	 * @return US sensor value
	 */
	public float[] getUSSensorSample() {
		float[] usSample = new float[ev3USSensor.sampleSize()];
		ev3USSensor.fetchSample(usSample, 0);
		return usSample;
	}
	
	/**
	 * 
	 * @return RGB value sample of EV3 color sensor
	 */
	public float[] getEV3ColorSample() {
		ev3ColorSensor.setCurrentMode("RGB");
		float[] ev3ColorSensorSample = new float[ev3ColorSensor.sampleSize()];
		ev3ColorSensor.fetchSample(ev3ColorSensorSample, 0);
//		for (float color : ev3ColorSensorSample) {
//			System.out.println(color);
//		}
//		System.out.println("---");
		return ev3ColorSensorSample;
	}
	
	/**
	 * 
	 * @return RGB value sample of NXT color sensor
	 */
	public float[] getNXTColorSample() {
		nxtColorSensor.setCurrentMode("RGB");
		float[] nxtColorSensorSample = new float[nxtColorSensor.sampleSize()];
		nxtColorSensor.fetchSample(nxtColorSensorSample, 0);
		for (float color : nxtColorSensorSample) {
			System.out.println(color);
		}
		System.out.println("---");
		return nxtColorSensorSample;
	}
	
	/**
	 * 
	 * @param colorSample sample of the EV3 color sensor in RGB mode
	 * @param colorThreshold RGB color range of specified LEGO block
	 * @return true if color sample values of EV3 color sensor are within the range of the specified block
	 */
	public boolean blockFound(float[] colorSample, BLOCK_COLOR_VALUES colorThreshold) {
		for (int i=0; i<colorThreshold.getRGBBlockValues().length; i++) {
			if (colorSample[i] <= colorThreshold.getRGBBlockValues()[i][0]
					|| colorSample[i] >= colorThreshold.getRGBBlockValues()[i][1]) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * 
	 * @param colorSample sample of the NXT color sensor in RGB mode
	 * @param colorThreshold RGB color range of specified area
	 * @return true if color sample values of NXT color sensor are within the range of the specified area
	 */
	public boolean areaFound(float[] colorSample, AREA_COLOR_VALUES color_THRESHOLD) {
		for (int i=0; i<color_THRESHOLD.getRGBAreaValues().length; i++) {
			if (colorSample[i] <= color_THRESHOLD.getRGBAreaValues()[i][0]
					|| colorSample[i] >= color_THRESHOLD.getRGBAreaValues()[i][1]) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Robot drives backwards and turns in random direction
	 * */
	public void driveBackwardsAndTurn() {
		leftMotor.backward();
		rightMotor.backward();
		Delay.msDelay(500);
		leftMotor.forward();
		rightMotor.backward();
		Delay.msDelay(ThreadLocalRandom.current().nextInt(500, 1500));
	}
	
	public Drive getDrive() {
		return new Drive(rightMotor, leftMotor, nxtColorSensor, ev3USSensor);
	}
	
	/**
	 * stops and close all motors
	 */
	public void stopMotors() {
//		setDrivingSpeed(10);
		leftMotor.stop();
		rightMotor.stop();
		rightMotor.close();
		leftMotor.close();
	}

	/**
	 * closes all sensors
	 */
	public void closeSensors() {
		ev3USSensor.close();
		ev3ColorSensor.close();
		nxtColorSensor.close();
		irSensor.close();
	}
}