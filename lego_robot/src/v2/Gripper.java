package v2;

import lejos.robotics.RegulatedMotor;

public class Gripper implements Runnable {
	
	private RegulatedMotor grabMotor;
	
	private final int grabMotorSpeed = 1000;
	private final int closedPosition = 4400; // about 3 Duplo bricks (~6cm)
	
	private boolean closeGripper;
	
    public Gripper(RegulatedMotor grabMotor, boolean closeGripper) {
    	this.grabMotor = grabMotor;
    	this.closeGripper = closeGripper;
    }

	@Override
	public void run() {
		grabMotor.setSpeed(grabMotorSpeed);
		
		if (closeGripper) { // close gripper
			grabMotor.rotateTo(closedPosition);
		} else {
			grabMotor.rotateTo(0);
		}
		
	}

}
