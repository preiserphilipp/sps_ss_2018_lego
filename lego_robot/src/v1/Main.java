package v1;

public class Main {
	
	// Hint: for stacktrace and other information see: "C:\Program Files\leJOS EV3\bin\ev3control.bat"

	public static void main(String[] args) {
		
		/*
		 * findings:
		 * 	-	leJOS works only with Java 7
		 * 	-	brick works only with a WiFi USB adapter
		 * 	-	gripper is very lose (needs to be better mounted)
		 * 	-	NXT color sensor does not work (freezes block)
		 * 	-	NXT light sensor is very inaccurate (color values changing constantly)
		 * 	-	brick freezes sometimes (remove batteries)
		 * 	-	sensors needs to be closed before using them again in another class
		 * 	-	brick itself is very loose (a rubber band helps)
		 * 	-	always go to the main menu of the robot before executing the project
		 */
		
		/*
		 * status:
		 * 		robot drives around randomly and searches for yellow blocks,
		 * 		in case a block is found, it searches for the target area
		 * 		-> search for target area is very unreliable because NXT light
		 *  	   sensor provides inconsistent values
		 */
		
		Robot robot = new Robot();
		
		/*try {
//			robot.colorSensorTests();
			robot.driveRandomAndSearchBlock(400);
			robot.stopMotors();
		} 
		finally {
			robot.closeSensors();
		}*/
	}

}
