package v1;

import lejos.hardware.motor.Motor;
import lejos.robotics.RegulatedMotor;

public class Gripper implements Runnable {
	
	private int GRAB_SPEED = 250;
	private int CLOSE_ANGLE = 90;
	private int TACHO_LIMIT = 50;
	
	private RegulatedMotor grabMotor = Motor.A;

	private boolean closeGripper;
    public Gripper(boolean closeGripper) {
       this.closeGripper = closeGripper;
    }
	
	/**
	 * Opens or closes the gripper depending on the current state.
	 */
	
	@Override
	public void run() {
		grabMotor.setSpeed(GRAB_SPEED);
		if (closeGripper) { // close gripper
			if (grabMotor.getTachoCount() < TACHO_LIMIT) { // if gripper is open...
				grabMotor.rotateTo(CLOSE_ANGLE); // move to closed position
			}
		} else { // open gripper
			if (grabMotor.getTachoCount() > TACHO_LIMIT) { // if gripper is closed...
				grabMotor.rotateTo(0); // move to open position
			}
		}
		grabMotor.stop();
	}

}
