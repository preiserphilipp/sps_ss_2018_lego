package v1;

import java.text.DecimalFormat;
import java.util.concurrent.ThreadLocalRandom;

import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.hardware.motor.Motor;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3IRSensor;
import lejos.hardware.sensor.NXTLightSensor;
import lejos.robotics.RegulatedMotor;
import lejos.utility.Delay;

public class Robot {
	
	// IR sensor constants
	private final int LEDGE_HEIGHT = 30;
	
	// grab motor constants
	private final int GRAB_SPEED = 200;
	private final int CLOSE_ANGLE = 90;
	private final int TACHO_LIMIT = 50;
	
	// EV3 color sensor constants
	private final double RED_CHANNEL_YELLOW_BLOCK_THRESHOLD = 0.05;
	private final double GREEN_CHANNEL_GREEN_BLOCK_THRESHOLD = 0.00; // no value yet
	
	// sensor is very inaccurate!
	// NXT light sensor constants
//	private final double RED_MODE_RED_PAPER_THRESHOLD = 0.42; // over
//	private final double AMBIENT_MODE_RED_PAPER_THRESHOLD = 0.41; // over
//	private final double RED_MODE_BLACK_PAPER_THRESHOLD = 0.4; // under
//	private final double RED_MODE_WHITE_PAPER_THRESHOLD = 0.485; // over
	
	private final double RED_MODE_AVOID_AREA = 0.0; // no value set yet

	// motors
	private RegulatedMotor leftMotor = Motor.B;
	private RegulatedMotor rightMotor = Motor.C;
	private RegulatedMotor grabMotor = Motor.A;
	
	// sensors
	private EV3IRSensor irSensor = new EV3IRSensor(SensorPort.S3);
	private EV3ColorSensor ev3ColorSensor = new EV3ColorSensor(SensorPort.S2);
	private NXTLightSensor nxtLightSensor = new NXTLightSensor(SensorPort.S1);
//	private NXTColorSensor foodColorSensor = new NXTColorSensor(SensorPort.S1); // does not work
	
	/**
	 * Test for color/light sensors. Displays color values of the sensors.
	 * 
	 * NXTColorSensor must be unplugged out during this test, otherwise
	 * the brick freezes. 
	 */
	public void colorSensorTests() {
		// modes: ColorID, Red, RGB, Ambient
		ev3ColorSensor.setCurrentMode("RGB"); // set mode
		float[] sample1 = new float[ev3ColorSensor.sampleSize()];
		for (int i=0; i<10; i++) {
			ev3ColorSensor.fetchSample(sample1, 0);
			System.out.println("R: " + sample1[0] + " G: " + sample1[1] + " B: " + sample1[2]); // sample output: R: 0.011764706 G: 0.018627452 B: 0.016666668
			Delay.msDelay(1000);
		}
		
		System.out.println("----------");
		
		// foodLightSensor apparently only works when fieldColorSensor was turned on beforehand
		// modes: Red, Ambient
		nxtLightSensor.setCurrentMode("Red"); // set mode
		float[] sample2 = new float[nxtLightSensor.sampleSize()];
		for (int i=0; i<10; i++) {
			nxtLightSensor.fetchSample(sample2, 0);
			System.out.println(sample2[0]); // sample output: 0.44908422
			Delay.msDelay(500);
		}
		
		ev3ColorSensor.close();
		nxtLightSensor.close();
		
		// does not work with the lejos sensor test tool and does also not work here
//		System.out.println(foodColorSensor.getColorID()); // DOES NOT WORK - this sensor freezes the brick (while sensor light is on)!
	}
	
	/**
	 * Robot drives around randomly. Checks regularly if the robot is
	 * about to drive of the edge. If so, the robot drives backwards and changes
	 * its angle randomly. 
	 * If a block is detected the gripper grabs it. The robot then searches for an 
	 * area with a certain color to place the block in it.
	 * 
	 * @param speed
	 *            Speed in which the robot should move
	 */
	public void driveRandomAndSearchBlock(int speed) {
		boolean programNotFinished = true;
		boolean yellowBlockFound = false;
		boolean greenBlockFound = false;
		boolean targetIsDarker = false;
		
		double NXTLightSensorGroundSample = 0.0;
		double NXTLightSensorTargetSample = 0.0;
		
		leftMotor.setSpeed(speed);
		rightMotor.setSpeed(speed);
		
		ev3ColorSensor.setCurrentMode("RGB");
		nxtLightSensor.setCurrentMode("Red");
		
		// close gripper (for better NXT light sensor readings)
		grab(true);
		
		// read in ground values
		System.out.println("Point NXT light sensor to the ground. \nPress ENTER when ready.\n");
		Button.waitForAnyPress();
		float[] NXTLightSensorGroundSamples = new float[nxtLightSensor.sampleSize()];
		nxtLightSensor.fetchSample(NXTLightSensorGroundSamples, 0);
		NXTLightSensorGroundSample = NXTLightSensorGroundSamples[0];
		LCD.clear();
		
		System.out.println("Point NXT light sensor to the target. \nPress ENTER when ready.\n");
		Button.waitForAnyPress();
		float[] NXTLightSensorTargetSamples = new float[nxtLightSensor.sampleSize()];
		nxtLightSensor.fetchSample(NXTLightSensorTargetSamples, 0);
		NXTLightSensorTargetSample = NXTLightSensorTargetSamples[0];
		LCD.clear();
		
		System.out.println("Press ENTER to start.");
		Button.waitForAnyPress();
		
		// for debugging purposes
		System.out.println("Initial values: ");
		System.out.println(NXTLightSensorGroundSample);
		System.out.println(NXTLightSensorTargetSample);
		
		// check if ground is lighter or darker then target
		if (NXTLightSensorGroundSample > NXTLightSensorTargetSample) {
			targetIsDarker = true;
		} else {
			targetIsDarker = false;
		}
		
		// open gripper
		grab(false);
		
		while (programNotFinished) {
			if (!IRSensor()) { // if no ledge
				
				float[] EV3ColorSensorSample = new float[ev3ColorSensor.sampleSize()];
				ev3ColorSensor.fetchSample(EV3ColorSensorSample, 0);
				
				if (!yellowBlockFound) { // if yellow block not yet found
					if (EV3ColorSensorSample[0] > RED_CHANNEL_YELLOW_BLOCK_THRESHOLD) { // yellow block detected
						System.out.println("YELLOW BLOCK found!");
						
						// for better synchronization
						Thread thread = new Thread(new Gripper(true)); // close gripper
						thread.start(); // start thread
						
						// if block is found, move backwards
						Delay.msDelay(500);
						leftMotor.backward();
						rightMotor.backward();
						Delay.msDelay(500);
						
						yellowBlockFound = true;
						
						// thread synchronization
						try {
							thread.join();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
				
				// not tested!
				if(!greenBlockFound) {
					ev3ColorSensor.fetchSample(EV3ColorSensorSample, 0);
					if (EV3ColorSensorSample[1] > GREEN_CHANNEL_GREEN_BLOCK_THRESHOLD) { // green block detected
						System.out.println("GREEN BLOCK found!");
						moveInRandomDirection();
					}
				}
				
				// drive forward
				leftMotor.forward();
				rightMotor.forward();
			}
			if (IRSensor()) { // ledge found
				moveInRandomDirection();
			}
			
			float[] NXTLightSensorSample = new float[nxtLightSensor.sampleSize()];
			nxtLightSensor.fetchSample(NXTLightSensorSample, 0);
			
			// if yellow block was found, search for target area
			if (yellowBlockFound) {
				DecimalFormat df = new DecimalFormat(".##"); // round number
				
				// for debugging purposes
				System.out.println(NXTLightSensorSample[0]);
				System.out.println("Target value: " + Double.parseDouble(df.format(NXTLightSensorTargetSample)));
				
//				double differenceGroundValues = NXTLightSensorSample[0] - NXTLightSensorGroundSample;

				// unreliable - NXT light sensor provides inconsistent values!!!
				if ((targetIsDarker && NXTLightSensorSample[0] <= Double.parseDouble(df.format(NXTLightSensorTargetSample))) 
						|| (!targetIsDarker && NXTLightSensorSample[0] >= Double.parseDouble(df.format(NXTLightSensorTargetSample)))) {
					System.out.println("TARGET FOUND! - on dark ground");
					Thread thread = new Thread(new Gripper(false)); // open gripper
					thread.start(); // start thread
					programNotFinished = false;
				}
			}
			
			// not tested 
			if (NXTLightSensorSample[0] > RED_MODE_AVOID_AREA) {
				moveInRandomDirection();
			}
			
			// break loop with ENTER
			if (Button.ENTER.isDown()) {
				System.out.println("Program was terminated by pressing a ENTER.");
				programNotFinished = false;
			}
		}
	}
	
	/**
	 * Move backwards and turn in random direction
	 */
	public void moveInRandomDirection() {
		leftMotor.backward();
		rightMotor.backward();
		Delay.msDelay(400);
		leftMotor.forward();
		rightMotor.backward();
		Delay.msDelay(ThreadLocalRandom.current().nextInt(500, 1500));
	}

	/**
	 * Closes all Sensors at the end of the Program
	 */
	public void closeSensors() {
		irSensor.close();
		ev3ColorSensor.close();
//		foodColorSensor.close();
		nxtLightSensor.close();
	}

	public void stopMotors() {
		leftMotor.setSpeed(10);
		rightMotor.setSpeed(10);
		leftMotor.stop();
		rightMotor.stop();
	}

	/**
	 * 
	 * @return true if the IR Sensor measures a distance > than the Ledge Height
	 */
	public boolean IRSensor() {
		float[] irSample = new float[3];
		irSensor.fetchSample(irSample, 0);
//		System.out.println("IRSensor: " + irSample[0]);
		return irSample[0] >= LEDGE_HEIGHT;
	}
	
	public void grab(boolean closeGripper) {
		grabMotor.setSpeed(GRAB_SPEED);
		if (closeGripper) { // close gripper
			if (grabMotor.getTachoCount() < TACHO_LIMIT) { // if gripper is open...
				grabMotor.rotateTo(CLOSE_ANGLE); // move to closed position
			}
		} else { // open gripper
			if (grabMotor.getTachoCount() > TACHO_LIMIT) { // if gripper is closed...
				grabMotor.rotateTo(0); // move to open position
			}
		}
		grabMotor.stop();
	}

}
